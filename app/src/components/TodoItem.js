import React from 'react';
import ReactDOM from 'react-dom';

export default class TodoItem extends React.Component {
  static propTypes = {
    deleteTodo: React.PropTypes.func.isRequired,
    editTodo: React.PropTypes.func.isRequired,
    saveEdit: React.PropTypes.func.isRequired,
    data: React.PropTypes.object.isRequired,
  };

  constructor(props){
    super(props);
    this.state = {
      editing: false,
      text: this.props.data.item,
    }
  }

  enableEdit = () => {
    const node = ReactDOM.findDOMNode(this.refs.edit);
    node.focus();
    this.setState({
      editing: true,
    });
  }
  cancelEdit = () => {
    this.setState({
      editing: false,
    });

    this.props.data.item = this.state.text;

    this.props.saveEdit(this.props.data);
  }
  
  editTodo = (e) => {
    this.setState({
      text: e.currentTarget.value,
    });
  }

  render() {
    return (
      <li
        className={this.state.editing ? 'editing' : '' || this.props.data.completed ? 'completed' : ''}
        onDoubleClick={this.enableEdit}
      >
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            onChange={() => this.props.completedTodo(this.props.data)}
            checked={this.props.data.completed}
          />
          <label>{this.props.data.item}</label>
          <button
            className="destroy"
            onClick={() => this.props.deleteTodo(this.props.data.objectId)}
          ></button>
        </div>
        <input
          className="edit"
          ref="edit"
          onChange={this.editTodo}
          onBlur={this.cancelEdit}
          value={this.state.text}
          autoFocus
        />
      </li>
    );
  }
}
