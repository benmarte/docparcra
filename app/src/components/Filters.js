import React from 'react';

export default class Filters extends React.Component {
  static propTypes = {
    remaining: React.PropTypes.number.isRequired,
    activeFilter: React.PropTypes.string.isRequired,
    showAll: React.PropTypes.func.isRequired,
    filterActive: React.PropTypes.func.isRequired,
    filterCompleted: React.PropTypes.func.isRequired,
    deleteCompleted: React.PropTypes.func.isRequired,
  };

  render() {
    return (
      <footer className="footer">
        <span className="todo-count"><strong>{this.props.remaining}</strong> item left</span>
        <ul className="filters">
          <li>
            <a className={this.props.activeFilter === 'all' ? 'selected' : null} onClick={this.props.showAll}>All</a>
          </li>
          <li>
            <a className={this.props.activeFilter === 'active' ? 'selected' : null} onClick={this.props.filterActive}>Active</a>
          </li>
          <li>
            <a className={this.props.activeFilter === 'completed' ? 'selected' : null} onClick={this.props.filterCompleted}>Completed</a>
          </li>
        </ul>
        <button className="clear-completed" onClick={this.props.deleteCompleted}>Clear completed</button>
      </footer>
    );
  }
}
