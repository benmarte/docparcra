import React, { Component } from 'react';
import Parse from 'parse';
import * as _ from 'lodash';
import './App.css';
import TodoItem from './components/TodoItem';
import Footer from './components/Footer';
import Filters from './components/Filters';

class App extends Component {
  constructor(props){
    super(props);
    Parse.initialize("Todo");
    Parse.serverURL = 'http://localhost:1337/parse';
    this.state = {
      todos: [],
      todo: '',
      filter: null,
      currentFilter: 'all',
    };
    this.query = new Parse.Query('Todo');
    this.subscription = this.query.subscribe();
  }  

  loadData = () => {
    const query = new Parse.Query('Todo');
    query.find({
      success: (results) => {
        let todos = [];
        results.map((todo) => {
          todos.push(todo.toJSON());
          return todos;
        });
        this.setState({
          todos,
        });
      },
      error: (error) => {
        console.warn(error);
      },
    });
  }

  todoText = (e) => {
    this.setState({
      todo: e.currentTarget.value,
    });
  }

  addTodo = (e) => {
    if (e.keyCode === 13) {
      const TodoItem = Parse.Object.extend('Todo');
      const todoItem = new TodoItem();
      todoItem.save({
        item: this.state.todo,
        completed: false,
      }).then(() => {
        this.loadData();
        this.setState({
          todo: '',
        })
      });
    }
  }

  deleteTodo = (todo) => {
    this.query.get(todo, {
      success: (item) => {
        item.destroy({
          success: (result) => {
            this.loadData();
          },
          error: (error) => alert(error),
        });
      },
      error: (error) => alert(error),
    });
  }

  completedTodo = (status) => {
    this.query.get(status.objectId, {
      success: (item) => {
        item.set('completed', !status.completed);
        item.save();
        this.loadData();
      }
    });
  }

  saveEdit = (status) => {
    this.query.get(status.objectId, {
      success: (item) => {
        item.set('item', status.item);
        item.save();
        this.loadData();
      }
    });
  }

  completeAll = () => {
    this.setState({
      completeAll: !this.state.completeAll,
    });
    const query = new Parse.Query('Todo');
    if (_.filter(this.state.todos, ['completed', false]).length > 0) {
      query.equalTo('completed', false);
      query.each(function(obj) {
        obj.set('completed', true);
        obj.save();
      },
      {success: () => this.loadData()},
      );
    } else {
      query.equalTo('completed', true);
      query.each(function(obj) {
        obj.set('completed', false);
        obj.save();
      },
      {success: () => this.loadData()},
      );
    }
  }

  showCompleted = () => {
    this.setFilter('completed');
  }

  showActive = () => {
    this.setFilter('active');
  }

  showAll = () => {
    this.setFilter('all');
  }

  setFilter = (filter) => {
    if (filter === 'completed') {
      this.setState({
        filter: ['completed', true],
        currentFilter: 'completed',
      });
    } else if(filter === 'active') {
      this.setState({
        filter: ['completed', false],
        currentFilter: 'active',
      });
    } else if (filter === 'all') {
      this.setState({
        filter: null,
        currentFilter: 'all',
      });
    }
  }

  removeCompleted = () => {
    const query = new Parse.Query('Todo');
    query.equalTo('completed', true);
    query.each(function(obj) {
        obj.destroy({});
      },
      {success: () => this.loadData()},
    );
  }

  componentDidMount() {
    this.loadData();
    this.subscription.on('create', (todo) => {
      // console.warn('Updated via create');
      // console.warn(todo);
      this.loadData();
    });
    this.subscription.on('update', (todo) => {
      // console.warn('Updated via update');
      // console.warn(todo);
      this.loadData();
    });
    this.subscription.on('delete', (todo) => {
      // console.warn('Updated via delete');
      // console.warn(todo);
      this.loadData();
    });
  }

  render() {
    return (
      <div className="App">
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              onChange={this.todoText}
              onKeyUp={this.addTodo}
              placeholder="What needs to be done?"
              value={this.state.todo}
            />
          </header>
          <section className="main">
            <input className="toggle-all" type="checkbox" onChange={this.completeAll} checked={_.filter(this.state.todos, ['completed', false]).length === 0}/>
            <label htmlFor="toggle-all">Mark all as complete</label>
            <ul className="todo-list">

              {_.filter(this.state.todos, this.state.filter).map((todo, index) => {
                return(
                  <TodoItem
                    key={index}
                    data={todo}
                    editTodo={() => this.editTodo(todo.item)}
                    deleteTodo={() => this.deleteTodo(todo.objectId)}
                    saveEdit={this.saveEdit}
                    completedTodo={this.completedTodo}
                  />
              );
              })}
            </ul>
          </section>
          <Filters 
            filterCompleted={this.showCompleted}
            filterActive={this.showActive}
            showAll={this.showAll}
            remaining={_.filter(this.state.todos, ['completed', false]).length}
            deleteCompleted={this.removeCompleted}
            activeFilter={this.state.currentFilter}
          />
        </section>
        <Footer />
      </div>
    );
  }
}

export default App;
