# React-Parse-Todo

Dockerized containers for creating a React web app with Parse as an API and Parse Dashboard to manage your Parse service.

## Prerequisites
- [Docker](http://www.docker.com/products/docker)

## What does this include?

- Docker container with [Parse-server](https://github.com/ParsePlatform/parse-server)
- Docker container with [Parse Dashboard](https://github.com/ParsePlatform/parse-dashboard)
- Docker container with [Mongo DB](https://www.mongodb.com/)
- Docker container with [Node](https://nodejs.org/en/) running [create-react-app](https://github.com/facebookincubator/create-react-app)

## How do I run this?

In the directory root directory of this repo you should have a `docker-compose.yml` file and an orchestration and an app folder.

Open terminal in this folder and run `docker-compose up` once docker completes creating the containers you should have your todo app running in `http://localhost:3000`

The source files to your React app will be located in: `app > src` folder

Your Mongo database will be stored in the `db` folder you can access your database via robomongo or any other database tool using `localhost:27017`

To access the Parse Dashboard navigate to: `http://localhost:4040` username: `admin` password: `12345`

## How to connect to docker containers

- To list all running containers in terminal type: `docker ps -a`
- To connect to the webapp containers terminal type: `docker exec -it todo-app bash`
- To connect to the mongodb containers terminal type: `docker exec -it mongo-todo bash`
- To connect to the parse-server containers terminal type: `docker exec -it parse-server-todo bash`
- To connect to the parse-dashboard containers terminal type: `docker exec -it parse-dashboard-todo bash`

## How to add packages to the webapp

- Connect to the todo-app container using the instructions above
- Once you are connected to the todo-app container cd into `/opt/todo`
- Install packages as you normally would using node `npm install package-name`